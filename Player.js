function Player(name){
    
    this.spawned = false;
  
    /** @var Vector2 */
    this.speed;
  
    this.x;
    this.y;
  
    this.connected = true;
    this.ip;
    this.port;
    this.username;
  
    this.usedChunks = [];
    this.loadQueue = [];
  

    this.getPlayer = function() {
        return (this);
    };

    this.isOnline = function() {
        return (this.connected);   
    };
    
    this.getAddress = function() {
        return (this.ip);
    };
  
    this.getPort = function() {
        return (this.port);
    };
}

exports.Player = Player;