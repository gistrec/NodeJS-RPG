Пакеты
======

### Сервер -> Клиент

| Название пакета  | Содержимое       | Назначение             |
| :--------------- |:---------------- | :--------------------- |
| login success    | (string) login   | Успешная авторизация   |
| login fail       | (string) error   | Неуспешная авторизация |
| register success | (string) login   | Успешная регистрация   |
| register fail    | (string) error   | Неуспешная регистрация |
| create player    | (object) Player  | Создание нового игрока |
| remove player    | (integer) Id     | Уничтожение игрока     |
| create entity    | (object) Entity  | Создание нового entity |
| remove entity    | (integer) Id     | Уничтожение entity     |
| update player    | (object) Player  | Обновление игрока      |
| update entity    | (object) Entity  | Обновление entity      |
| new message      | (object) Message | Новое сообщение        |
| init map         | (object) Level   | Уровень                |

### Клиент -> Сервер


Object
=====
### Player
| Название       | Обозначение | Тип переменной |
| :------------- | :---------- | :------------- |
| Айди           | id          | integer        |
| Имя/Логин      | name        | string         |
| Координата x   | x           | integer        |
| Координата y   | y           | integer        |
| Направление    | dir         | integer        |
| Макс. здоровье | maxHealth   | integer        |
| Текущ. здоровье| health      | integer        |