var Server = require('../Server.js');

/*  Entity Type:
 *  0 - wall
 *  1 - player
 */

function Entity(id, type) {
	this.type = type;
	this.id = id;

	this.x;
	this.y;

	this.maxHealth = 100;
	this.health = 100;
	//this.strength = 5;
	//this.level = 1;
	//this.killTime = 0;
	this.spawned = false;
	//this.def = 10;
	//this.hitSpeed = 900;
	//this.respawnTime = 3000;
	//this.fightingPlayers = [];
	//this.lastStrike = 0;
	//this.size = 32;
	//this.hitArray = [];

    this.spawn = function() {
    	this.spawn = true;
    	Server.spawn(this);
    }

    this.despawn = function() {
    	this.spawn = false;
    	Server.despawn(this);
    }

    this.setPosition = dunction(x, y) {
    	this.x = x;
    	this.y = y;
    }	

	/*this.getHurt = function(amount) {
		this.currhp -= amount;
		this.hitArray.push([amount, 1.0]);
		if(this.currhp < 0) {
			this.alive = false;
			this.currhp = 0;
			this.killTime = Date.now();
			this.fightingPlayers = [];
			this.hitArray = [];
		}
	};*/

	/*this.readyToHit = function() {
		return (this.fightingPlayers.length > 0 && (Date.now() - this.lastStrike > this.hitSpeed));
	};

	this.killedPlayer = function() {
		this.fightingPlayers.shift();
	};

	this.fightingAgainst = function() {
		return (this.fightingPlayers.length > 0) ? this.fightingPlayers[0] : null;
	};

	this.setLastStrike = function(time) {
		this.lastStrike = time;
	};*/


	/*this.startFight = function(playerID) {
		this.fightingPlayers.push(playerID);
	}

	this.stopFight = function() {
		this.fightingPlayers = [];
	}; */
}

// Export the Entity class so you can use it in
// other files by using require("Entity").Entity
exports.Entity = Entity;
