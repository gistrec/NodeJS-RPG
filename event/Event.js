var Server = require('../Server.js')

function Event(client) {

    // Сначала описание функций, а потом уже работа с эвентами

    PlayerLoginEvent = function(data) {
        console.log("Игрок " + data.login + " залогинился");
        this.emit("login success", data.login);
        Server.joinPlayer(this, data.login);
        /*db.users.findOne({"name": login}, function(err, savedUser) {
            if(err || !savedUser) {
                toClient.emit("login fail", login);
            }else {
                toClient.emit("login success", login);
                joinPlayer(toClient, login);
            }
        });*/
    }

    PlayerRegisterEvent = function(data){
        console.log("Игрок " + data.login + " зарегестрировался");
        this.emit("register success", data.login);
        Server.joinPlayer(toClient, data.login);
        /*db.users.findOne({"name": login}, function(err, savedUser) {
            if(!savedUser) {
                var player = new Player(128, 192, login, passwd, 100);
                db.users.save(player, function(err2, savedUser2) {
                    if(!err2 && savedUser2) {
                        toClient.emit("register success", login);
                        joinPlayer(toClient, login);
                    }else{
                        util.log(err2);
                    }
                });
            }else {
                toClient.emit("register fail", login);
            }
        });*/
    }

    PlayerUpdateEvent = function(data) {
        var player = Server.getPlayerById(this.id);
        if (!player) return false; // If Player is not fount
        // Update player position
        player.pos.x = data.x;
        player.pos.y = data.y;
        player.dir = data.dir;
        // Check if player stepped on an item
        /*var item = getItem(player.pos.x, player.pos.y);
        if(item) {
            io.sockets.emit("update item", {item: item, remove: true}); // TO-DO: only player who got the item should be informed
            io.sockets.emit("get item", {item: item, quantity: item.quantity});
            player.takeItem(item.type, item.quantity);
            removeItem(player.pos.x, player.pos.y);
        }*/
        // Broadcast updated position to connected socket clients
        this.broadcast.emit("update player", player);
        /*if(data.enemyID != null) {
            player.fighting = data.enemyID;
            enemies[data.enemyID].startFight(data.id);
        }*/
    }

    SendMessageEvent = function(data) {
        var player = Server.getPlayerById(this.id);
        if (data.chatTo) {
            var chatTo = Server.getPlayerByName(data.chatTo);
            if (data.mode == "w" && chatTo) {
                Server.io.to(chatTo.id).emit("new message", {player: player.name, text: data.text, mode: data.mode});
            }else if (!chatTo) {
                this.emit("new message", {player: data.chatTo, text: "Player " + data.chatTo + " doesn't exist!", mode: "s"});
            }
        }else {
            Server.io.sockets.emit("new message", {player: player.name, text: data.text, mode: data.mode});
        }
    }

    PlayerDisconnectEvent = function(data) {
        var player = Server.getPlayerById(this.id);
        if(!player) return;
        this.broadcast.emit("new message", {player: player.name, text: " left the game", mode: "s"});
        Server.players.splice(this.id, 1);
        this.broadcast.emit("remove player", {id: this.id});
    }

    PlayerLogoutEvent = function(data) {
        var player = Server.getPlayerById(this.id);
        if(!player) return;
        this.broadcast.emit("new message", {player: player.name, text: " left the game", mode: "s"});
        Server.players.splice(this.id, 1);
        this.broadcast.emit("remove player", {id: this.id});
    }
    
    client.on("player login", PlayerLoginEvent);
    client.on("player register", PlayerRegisterEvent);
    client.on("update player", PlayerUpdateEvent);
    client.on("new message", SendMessageEvent);
    client.on("disconnect", PlayerDisconnectEvent);        
    client.on("logout", PlayerLogoutEvent);
    //client.on("start fight", StartFightEvent);
    //client.on("abort fight", AbortFightEvent);
}
exports.Event = Event;